let posts = [];
let count =1;

// Add Post
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault();

    posts.push({
    id:count,
    title: document.querySelector('#text-title').value,
    body: document.querySelector('#text-body').value

});

count++;
showPost(posts)


})

// SHOW POST
const showPost = (posts) =>{
    let postEntries='';
    posts.forEach((post) => {
        postEntries += 
        `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost ('${post.id}')">Delete</button>
        </div>
        `;
    });
    document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit Post

const editPost = (id)=>{
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#text-edit-id').value = id;
    document.querySelector('#text-edit-title').value = title;
    document.querySelector('#text-edit-body').value = body;

}


const deletePost = (id)=>{
    
    let del = (`${id}`)
    let deleteDiv =document.querySelector(`#post-${id}`)
    for (let i=0; i < posts.length; i++){
        if(posts[i].id.toString() === del){
            
            posts.splice(i,1)     
            deleteDiv.remove()
            console.log(posts)
  
        }
    
    }
}


// UPDATE
document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
    e.preventDefault()

    let see = document.querySelector('#text-edit-id').value
    console.log(see)
    for(let i = 0; i< posts.length; i++){
        if(posts[i].id.toString() === document.querySelector('#text-edit-id').value)
        {
            posts[i].title = document.querySelector('#text-edit-title').value;
            posts[i].body = document.querySelector('#text-edit-body').value;
            showPost(posts)
            alert("Movie Updated")
            
            break;
        }
    }
})